from django import forms
from recipes.models import Rating

# except Exception:
# pass


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]
